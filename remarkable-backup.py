import filecmp
import json
import os
import urllib.request
from typing import Dict

import paramiko

# download url
REMARKABLE_DOWNLOAD_URL = "http://{host}/download/{uuid}/placeholder"
# location of the metadata files on the remarkable table
REMARKABLE_METADATA_REMOTE = "/home/{user}/.local/share/remarkable/xochitl"
# default name for the metadata folder
REMARKABLE_METADATA_LOCAL = ".metadata"


class Entry:
    def __init__(self, name: str, parent: str, update: bool = False) -> None:
        self.name = name
        self.parent = parent
        self.update = update

    def __repr__(self) -> str:
        return "{}".format(self.name)


def download_metadata(
    directory: str, user: str = "root", host: str = "10.11.99.1", port: str = "22"
) -> Dict[str, Entry]:
    """
    :arg directory: output directory.
    :arg user: username used to connect to the tablet through SSH.
    :arg host: host name.
    :arg port: default port.

    :return: a `dict` of :class:`Entry`s with potentially new or out of
        date files.
    """

    # create local path
    metadata_directory = os.path.join(directory, REMARKABLE_METADATA_LOCAL)
    if not os.path.exists(metadata_directory):
        os.makedirs(metadata_directory)
    print("metadata directory: {}".format(metadata_directory))

    # connect to remarkable
    print("ssh: connecting to remarkable")
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(host, username=user, port=port)
    except Exception:
        import getpass

        print("ssh: device password is found in rM/About")
        password = getpass.getpass("password {}@{}: ".format(user, host))

        client.connect(host, username=user, port=port, password=password)
    print("ssh: connected")

    # copy metadata files
    print("ssh: copying metadata files")
    sftp = client.open_sftp()

    metadata = {}
    remote_directory = REMARKABLE_METADATA_REMOTE.format(user=user)
    for path in sftp.listdir(remote_directory):
        if not path.endswith(".metadata"):
            continue

        # download file
        remote_path = os.path.join(remote_directory, path)
        local_path = os.path.join(metadata_directory, path)
        if os.path.exists(local_path):
            local_path_new = "{}.new".format(local_path)
        else:
            local_path_new = local_path
        sftp.get(remote_path, local_path_new)

        # check if the metadata is different the the local one
        add_metadata = True
        if local_path_new != local_path:
            if filecmp.cmp(local_path_new, local_path, shallow=False):
                add_metadata = False
                os.unlink(local_path_new)

        if not add_metadata:
            continue

        # only keep documents, not folders
        with open(local_path_new) as f:
            info = json.load(f)

        if info["type"] != "DocumentType":
            continue

        # add the metadata
        uuid = os.path.basename(path).split(".")[0]
        entry = Entry(info["visibleName"], info["parent"])
        metadata[uuid] = entry

    sftp.close()
    client.close()

    return metadata


def filter_metadata(directory: str, metadata: Dict[str, Entry]) -> Dict[str, Entry]:
    """
    :arg directory: output directory.
    :arg metadata: a ``dict`` of :class:`Entry`s returned by
        :func:`donwload_metadata`.

    :return: a ``dict`` of :class:`Entry` where the parent has been updated
        to the full relative path and any existing files have been filtered
        out.
    """

    # create local path
    metadata_directory = os.path.join(directory, REMARKABLE_METADATA_LOCAL)

    # get folders by uuid
    folders = {}
    for path in os.listdir(metadata_directory):
        with open(os.path.join(metadata_directory, path)) as f:
            info = json.load(f)

        if info["type"] == "DocumentType":
            continue

        uuid = path.split(".")[0]
        folders[uuid] = Entry(info["visibleName"], info["parent"])

    # filter out old files
    metadata_ = {}
    for uuid in metadata:
        path_old = os.path.join(metadata_directory, "{}.metadata".format(uuid))
        path_new = os.path.join(metadata_directory, "{}.metadata.new".format(uuid))

        # if we have a completely new file, just add it to the list
        if not os.path.exists(path_new):
            metadata_[uuid] = metadata[uuid]
            continue

        # otherwise check if the "new" version is actually new
        with open(path_new) as f:
            version_new = int(json.load(f)["version"])
        with open(path_old) as f:
            version_old = int(json.load(f)["version"])

        if version_new <= version_old:
            os.unlink(path_new)
            continue

        # add the actually new version
        os.replace(path_new, path_old)

        metadata_[uuid] = metadata[uuid]
        metadata_[uuid].update = True
    metadata = metadata_

    # add full parent paths
    for uuid in metadata:
        parent = ""
        key = metadata[uuid].parent
        while key != "":
            parent = os.path.join(folders[key].name, parent)
            key = folders[key].parent

        metadata[uuid].parent = os.path.join(directory, parent)

    # filter out existing files
    metadata_ = {}
    for uuid in metadata:
        if metadata[uuid].update:
            metadata_[uuid] = metadata[uuid]
            continue

        if not os.path.exists(metadata[uuid].parent):
            metadata_[uuid] = metadata[uuid]
            continue

        file_exists = False
        for filename in os.listdir(metadata[uuid].parent):
            if filename.startswith(metadata[uuid].name):
                file_exists = True
                break

        if not file_exists:
            metadata_[uuid] = metadata[uuid]

    metadata = metadata_
    return metadata


def download_files(
    directory: str, metadata: Dict[str, Entry], host: str = "10.11.99.1"
) -> None:
    """
    :arg directory: output directory.
    :arg host: host name.
    :arg metadata: metadata of the files that should be downloaded.
        See :func:`download_metadata`.
    """
    from urllib.error import HTTPError

    nfiles = len(metadata)
    for count, uuid in enumerate(metadata):
        try:
            os.makedirs(metadata[uuid].parent)
        except OSError:
            pass

        url = REMARKABLE_DOWNLOAD_URL.format(host=host, uuid=uuid)
        try:
            with urllib.request.urlopen(url) as request:
                filename = metadata[uuid].name
                if not os.path.splitext(filename)[1]:
                    filename = "{}.pdf".format(filename)
                path = os.path.join(metadata[uuid].parent, filename)

                with open(path, "wb") as f:
                    f.write(request.read())
        except HTTPError:
            print("[{:05} / {:05}] HTTPError: {} ".format(count + 1, nfiles, url))
        except ConnectionResetError:
            print("[{:05} / {:05}] Error: {} ".format(count + 1, nfiles, url))
        else:
            print(
                "[{:05} / {:05}] downloaded: {}".format(
                    count + 1, nfiles, metadata[uuid].name
                )
            )

    print("backup: up to date.")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--user", default="root")
    parser.add_argument("--host", default="10.11.99.1")
    parser.add_argument("--port", default="22")
    parser.add_argument("--directory", default="remarkable")
    parser.add_argument("--dry-run", action="store_true")
    args = parser.parse_args()

    metadata = download_metadata(
        args.directory, user=args.user, host=args.host, port=args.port
    )
    metadata = filter_metadata(args.directory, metadata)

    if args.dry_run:
        nfiles = len(metadata)
        for count, uuid in enumerate(metadata):
            path = os.path.join(
                args.directory, metadata[uuid].parent, metadata[uuid].name
            )

            print("[{:05} / {:05}] download: {}".format(count + 1, nfiles, path))

    else:
        download_files(args.directory, metadata, host=args.host)
