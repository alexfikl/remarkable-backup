reMarkable Backup
=================

Requires:

* [paramiko](https://github.com/paramiko/paramiko) for connecting to
the tablet using SSH.

The script works in three steps:

* it connects to the tablet using SSH (on the default `10.11.99.1`)
and copies all the metadata files into a local `.metadata` directory. These
are useful because they contain actual file names, directory names, etc.
* it checks the metadata files against existing information and filters
them out if the version of the file is the same or older. This allows updating
existing files.
* it downloads the PDF files from the web interface at the URL
`http://10.11.99.1/download/some-file-uuid/placeholder`.

Most variables are configurable, see:
```
remarkable-backup.py --help
```
If a sync failed, the easiest way to restart is to delete the metadata files
and let the script try again.

This may require some setup on the reMarkable itself

* Go to `Storage` and make sure the USB interface is turned on
* On Linux, the connection shows up as a wired network, so this needs to be
  connected as well.
* Should now be able to go to `http://10.11.99.1` and have it actually load.
